# Bitbucket LFS Media Adapter

Take Git LFS to the next level

This custom transfer adapter for [Git LFS](https://git-lfs.github.com) speeds up uploads and downloads to/from [Bitbucket Cloud](https://bitbucket.org).

**Please Note**: This is an automatically updated package. If you find it is out of date by more than a day or two, please contact the maintainer(s) and let them know the package is no longer updating correctly.

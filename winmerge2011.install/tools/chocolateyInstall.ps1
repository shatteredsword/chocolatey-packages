﻿#NOTE: Please remove any commented lines to tidy up prior to releasing the package, including this one

#Items that could be replaced based on what you call chocopkgup.exe with
#{{PackageName}} - Package Name (should be same as nuspec file and folder) |/p
#{{PackageVersion}} - The updated version | /v
#{{DownloadUrl}} - The url for the native file | /u
#{{PackageFilePath}} - Downloaded file if including it in package | /pp
#{{PackageGuid}} - This will be used later | /pg
#{{DownloadUrlx64}} - The 64bit url for the native file | /u64

$packageName = 'winmerge2011.install' # arbitrary name for the package, used in messages
$installerType = 'exe' #only one of these: exe, msi, msu
$url = '{{DownloadUrl}}' # download url
$url64 = '{{DownloadUrlx64}}'  # 64bit URL here or remove - if installer decides, then use $url
$checksum = '{{Checksum}}'
$checksum64 = '{{Checksumx64}}'
$silentArgs = '/S' # "/s /S /q /Q /quiet /silent /SILENT /VERYSILENT" # try any of these to get the silent installer #msi is always /quiet
$validExitCodes = @(0) #please insert other valid exit codes here, exit codes for ms http://msdn.microsoft.com/en-us/library/aa368542(VS.85).aspx

try {
  $installFile = Join-Path $env:TEMP "$packageName" "WinMerge2011_{{PackageVersion}}.exe"

  # downloader that the main helpers use to download items
  Get-ChocolateyWebFile "$packageName" -FileFullPath "$installFile" `
                        -Url "$url" -Checksum "$checksum" -ChecksumType sha256 `
                        -Url64 "$url64" -Checksum64 "$checksum64" -ChecksumType64 sha256
  # installer, will assert administrative rights - used by Install-ChocolateyPackage
  Install-ChocolateyInstallPackage "$packageName" "$installerType" "$silentArgs" "$installFile" -validExitCodes $validExitCodes

  # the following is all part of error handling
  Write-ChocolateySuccess "$packageName"
} catch {
  Write-ChocolateyFailure "$packageName" "$($_.Exception.Message)"
  throw
}
